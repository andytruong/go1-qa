<?php

$databases['default']['default'] = [
    'driver'   => 'mysql',
    'database' => 'drupal',
    'username' => 'root',
    'password' => $_ENV['DB_INSTANCE_LOCAL_ENV_MYSQL_ROOT_PASSWORD'],
    'host'     => $_ENV['DB_INSTANCE_LOCAL_PORT_3306_TCP_ADDR'],
    'port'     => $_ENV['DB_INSTANCE_LOCAL_PORT_3306_TCP_PORT'],
];
